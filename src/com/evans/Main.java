package com.evans;

import java.util.Scanner;

public class Main {


    public static int num1 = 0;
    public static int num2 = 0;
    public static int total = 0;
    public static int remainder = 0;
    public static boolean again = false;
    public static char againChar;

    public static void getNumbers() {
        do {
            //declare variables
            boolean error = false;
            String input;
            Scanner in = new Scanner(System.in); //initiate scanner;
            System.out.println("This program will collect 2 numbers and then it will divide the two numbers");
            do {
                System.out.print("Enter first whole number: ");
                input = in.nextLine();
                try {
                    num1 = Integer.parseInt(input);
                    error = false;
                } catch (NumberFormatException e) {
                    System.out.println(input + " is not a valid whole number. Please try again");
                    error = true;
                }
            } while (error);
            do {
                System.out.print("Enter second whole number: ");
                input = in.nextLine();
                try {
                    num2 = Integer.parseInt(input);
                    if (num2 == 0) {
                        System.out.println("Cannot Divide by zero. Please try again.");
                        error = true;
                    } else {
                        error = false;
                    }
                } catch (NumberFormatException e) {
                    System.out.println(input + " is not a valid whole number. Please try again");
                    error = true;
                }
            } while (error);
            total = num1 / num2;
            remainder = num1 % num2;
            if (remainder > 0) {
                System.out.println(num1 + " / " + num2 + " = " + total + " remainder " + remainder);
            } else {
                System.out.println(num1 + " / " + num2 + " = " + total);
            }
            System.out.println();
            do {
                System.out.print("try again? (y,n) : ");
                input = in.nextLine();
                input = input.toLowerCase();
                if (input.length() > 0) {
                    againChar = input.charAt(0);
                    if (againChar == 'y') {
                        again = true;
                        error = false;
                    } else if (againChar == 'n') {
                        again = false;
                        error = false;
                    } else {
                        error = true;
                        System.out.println(againChar + " is not a valid response");
                    }
                } else {
                    error = true;
                    System.out.println(input + " is not a valid response");
                }
            } while (error);
            System.out.println();
        } while (again);
    }

    public static void main(String[] args) {
        getNumbers(); //call getNumbers() method and divide.
    }
}


